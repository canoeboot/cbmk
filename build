#!/usr/bin/env sh
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2014,2015,2020,2021,2023 Leah Rowe <leah@libreboot.org>
# SPDX-FileCopyrightText: 2015 Patrick "P. J." McDermott <pj@pehjota.net>
# SPDX-FileCopyrightText: 2015, 2016 Klemens Nanni <contact@autoboot.org>
# SPDX-FileCopyrightText: 2022, Caleb La Grange <thonkpeasant@protonmail.com>

set -u -e

export LC_COLLATE=C
export LC_ALL=C

. "include/err.sh"
. "include/option.sh"

eval "$(setvars "" option aur_notice tmpdir)"

tmpdir_was_set="y"
set | grep TMPDIR 1>/dev/null 2>/dev/null || tmpdir_was_set="n"
if [ "${tmpdir_was_set}" = "y" ]; then
	[ "${TMPDIR%_*}" = "/tmp/cbmk" ] || tmpdir_was_set="n"
fi
if [ "${tmpdir_was_set}" = "n" ]; then
	export TMPDIR="/tmp"
	tmpdir="$(mktemp -d -t cbmk_XXXXXXXX)"
	export TMPDIR="${tmpdir}"
else
	export TMPDIR="${TMPDIR}"
	tmpdir="${TMPDIR}"
fi

linkpath="${0}"
linkname="${linkpath##*/}"
buildpath="./script/${linkname}"

main()
{
	xx_ id -u 1>/dev/null 2>/dev/null
	[ $# -lt 1 ] && fail "Too few arguments. Try: ${0} help"

	[ "$1" = "dependencies" ] && xx_ install_packages $@ && cbmk_exit 0

	for cmd in initcmd check_git check_project git_init excmd; do
		eval "${cmd} \$@"
	done
	cbmk_exit 0
}

initcmd()
{
	[ "$(id -u)" != "0" ] || fail "this command as root is not permitted"

	check_project

	case "${1}" in
	help) usage ${0} ;;
	list) items "${buildpath}" ;;
	version) mkversion ;;
	*)
		option="${1}"
		return 0 ;;
	esac
	cbmk_exit 0
}

install_packages()
{
	if [ $# -lt 2 ]; then
		printf "You must specify a distro, namely:\n" 1>&2
		printf "Look at files under config/dependencies/\n" 1>&2
		printf "Example: ./build dependencies debian\n" 1>&2
		fail "install_packages: target not specified"
	fi

	[ -f "config/dependencies/${2}" ] || fail "Unsupported target"

	. "config/dependencies/${2}"

	xx_ ${pkg_add} ${pkglist}
	[ -z "${aur_notice}" ] && return 0
	printf "You must install AUR packages: %s\n" "$aur_notice" 1>&2
}

# release archives contain .gitignore, but not .git.
# cbmk can be run from cbmk.git, or an archive.
git_init()
{
	[ -L ".git" ] && fail "Reference .git is a symlink"
	[ -e ".git" ] && return 0
	eval "$(setvars "$(date -Rd @${versiondate})" cdate _nogit)"

	git init || fail "${PWD}: cannot initialise Git repository"
	git add -A . || fail "${PWD}: cannot add files to Git repository"
	git commit -m "${projectname} ${version}" --date "${cdate}" \
	    --author="cbmk <cbmk@canoeboot.org>" || \
	    fail "$PWD: can't commit ${projectname}/${version}, date $cdate"
	git tag -a "${version}" -m "${projectname} ${version}" || \
	    fail "${PWD}: cannot git-tag ${projectname}/${version}"
}

excmd()
{
	cbmkcmd="${buildpath}/${option}"
	[ -f "${cbmkcmd}" ] || fail "Invalid command. Run: ${linkpath} help"
	shift 1; "$cbmkcmd" $@ || fail "excmd: ${cbmkcmd} ${@}"
}

usage()
{
	progname=${0}
	cat <<- EOF
	$(mkversion)

	USAGE:	${progname} <OPTION>

	possible values for 'OPTION':
	$(items "${buildpath}")

	To know what ${projectname} version you're on, type:
	${progname} version

	Refer to ${projectname} documentation for more info.
	EOF
}

mkversion()
{
	printf "revision: %s %s\n" "$projectname" "$version"
	printf "revision date: %s\n" "$(date -Rud @${versiondate})"
}

cbmk_exit()
{
	tmp_cleanup || err "cbmk_exit: can't rm tmpdir upon exit $1: $tmpdir"
	exit $1
}

fail()
{
	tmp_cleanup || printf "WARNING: can't rm tmpdir: %s\n" "$tmpdir" 1>&2
	err "${1}"
}

tmp_cleanup()
{
	[ "${tmpdir_was_set}" = "n" ] || return 0
	rm -Rf "${tmpdir}" || return 1
}

main $@
