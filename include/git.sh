# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020,2021,2023,2024 Leah Rowe <leah@libreboot.org>
# SPDX-FileCopyrightText: 2022 Caleb La Grange <thonkpeasant@protonmail.com>

# This file is only used by update/project/trees

eval "$(setvars "" _target rev _xm loc url bkup_url depend tree_depend)"

fetch_project_trees()
{
	_target="${target}"
	[ -d "src/${project}/${project}" ] || fetch_from_upstream
	fetch_config
	if [ -d "src/${project}/${tree}" ]; then
		printf "download/%s %s (%s): exists\n" \
		    "${project}" "${tree}" "${_target}" 1>&2
		return 0
	fi
	prepare_new_tree
}

fetch_from_upstream()
{
	[ -d "src/${project}/${project}" ] && return 0

	x_ mkdir -p "src/${project}"
	fetch_project_repo "${project}"
}

fetch_config()
{
	rm -f "${cfgsdir}/"*/seen || err "fetch_config ${cfgsdir}: !rm seen"
	while true; do
		eval "$(setvars "" rev tree)"
		_xm="fetch_config ${project}/${_target}"
		load_target_config "${_target}"
		[ "${_target}" = "${tree}" ] && break
		_target="${tree}"
	done
	[ -n "$tree_depend" ] && [ "$tree_depend" != "$tree" ] && \
		x_ ./update trees -f "$project" "$tree_depend"; return 0
}

load_target_config()
{
	[ -f "$cfgsdir/$1/target.cfg" ] || err "$1: target.cfg missing"
	[ -f "${cfgsdir}/${1}/seen" ] && \
		err "${_xm} check: infinite loop in tree definitions"

	. "$cfgsdir/$1/target.cfg" || err "load_target_config !$cfgsdir/$1"
	touch "$cfgsdir/$1/seen" || err "load_config $cfgsdir/$1: !mk seen"
}

prepare_new_tree()
{
	printf "Creating %s tree %s (%s)\n" "$project" "$tree" "$_target"

	cp -R "src/${project}/${project}" "${tmpgit}" || \
	    err "prepare_new_tree ${project}/${tree}: can't make tmpclone"
	git_prep "$PWD/$cfgsdir/$tree/patches" "src/$project/$tree" "update"
	nukeblobs "$project/$tree" "$project/$tree"
}

fetch_project_repo()
{
	scan_config "${project}" "config/git" "err"
	[ -z "${loc+x}" ] && err "fetch_project_repo $project: loc not set"
	[ -z "${url+x}" ] && err "fetch_project_repo $project: url not set"

	clone_project
	[ -z "${depend}" ] || for d in ${depend} ; do
		x_ ./update trees -f ${d}
	done
	rm -Rf "${tmpgit}" || err "fetch_repo: !rm -Rf ${tmpgit}"

	for x in config/git/*; do
 		[ -f "${x}" ] && nukeblobs "${x##*/}" "src/${x##*/}"; continue
	done
}

clone_project()
{
	loc="${loc#src/}"
	loc="src/${loc}"
	if [ -d "${loc}" ]; then
		printf "%s already exists, so skipping download\n" "$loc" 1>&2
		return 0
	fi

	git clone $url "$tmpgit" || git clone $bkup_url "$tmpgit" \
	    || err "clone_project: could not download ${project}"
	git_prep "$PWD/config/$project/patches" "$loc"
}

git_prep()
{
	_patchdir="$1"
	_loc="$2"

	[ -z "${rev+x}" ] && err "git_prep $_loc: rev not set"
	git -C "$tmpgit" reset --hard $rev || err "git -C $_loc: !reset $rev"
	git_am_patches "$tmpgit" "$_patchdir" || err "!am $_loc $_patchdir"

	[ $# -lt 3 ] || [ ! -f "$tmpgit/.gitmodules" ] || \
	    git -C "$tmpgit" submodule update --init || err "prep $_loc: !mod"

	[ "$_loc" = "${_loc%/*}" ] || x_ mkdir -p "${_loc%/*}"
	mv "$tmpgit" "$_loc" || err "git_prep: !mv $tmpgit $_loc"
}

git_am_patches()
{
	for _patch in "$2/"*; do
		[ -L "$_patch" ] || [ ! -f "$_patch" ] || git -C "$1" am \
		    "$_patch" || err "git_am $1 $2: !git am $_patch"; continue
	done
	for _patches in "$2/"*; do
		[ ! -L "$_patches" ] && [ -d "$_patches" ] && \
			git_am_patches "$1" "$_patches"; continue
	done
}

# can delete from multi- and single-tree projects.
# called from build_projects() and handle_src_tree() on script/update/trees
nukeblobs()
{
	del="n"
	pjcfgdir="${1%/}"
	pjsrcdir="${2%/}"
	pjsrcdir="${pjsrcdir#src/}"
	[ ! -f "config/${pjcfgdir}/blobs.list" ] && return 0

	while read -r blobfile; do
		rmf="$(realpath "src/${pjsrcdir}/${blobfile}" 2>/dev/null)" || \
		    continue
		[ -L "${rmf}" ] && continue # we will delete the actual file
		[ "${rmf#${PWD}/src/${pjsrcdir}}" = "${rmf}" ] && continue
		[ "${rmf#${PWD}/src/}" = "${pjsrcdir}" ] && continue
		rmf="${rmf#${PWD}/}"
		[ -e "${rmf}" ] || continue
		del="y"
		rm -Rf "${rmf}" || \
		    err "nukeblobs ${pjcfgdir}/blobs: can't rm \"${blobfile}\""
		printf "nukeblobs %s: deleted \"%s\"\n" "${pjcfgdir}" "${rmf}"
	done < "config/${pjcfgdir}/blobs.list"

	[ "${del}" = "y" ] && return 0
	printf "nukeblobs %s: no defined blobs exist in dir, src/%s\n" 1>&2 \
	    "${pjcfgdir}" "${pjsrcdir}"
	printf "(this is not an error)\n"
}
