Canoeboot
=========

Find canoeboot documentation at <https://canoeboot.org/>

The `canoeboot` project provides
[libre](https://writefreesoftware.org/) *boot
firmware* that initializes the hardware (e.g. memory controller, CPU,
peripherals) on specific Intel/AMD x86 and ARM targets, which
then starts a bootloader for your operating system. Linux/BSD are
well-supported. It replaces proprietary BIOS/UEFI firmware. Help is available
via [\#canoeboot IRC](https://web.libera.chat/#canoeboot)
on [Libera](https://libera.chat/) IRC.

Canoeboot is maintained in parallel with Libreboot, by the same developer.
Strictly speaking, it is a *fork* of Libreboot, but with a twist:

Canoeboot is provided for the purists who absolutely wish to have no proprietary
software of any kind. Regardless of any other firmware that exists outside of it,
the boot flash on your system will be *entirely free software* if you install
Canoeboot on it. That includes a complete lack of CPU microcode updates, as per
FSF policy.

More specifically: Canoeboot is engineered to comply with the GNU Free System
Distribution Guidelines. It has, as of November 2023 releases, been strictly
audited by FSF licensing staff (Craig Topham lead the audit), and it is listed
on the FSF's own Free Software Directory.

Libreboot previously complied with that same policy, but changed to a different
one permitting binary blobs in limited circumstances, so as to support more newer
machines. Canoeboot is, then, a continuation of the traditional Libreboot
project prior to that policy change. Some users still want it, so, Canoeboot
releases are rigoriously maintained, re-basing on newer Libreboot releases over
time, just like how, say, Trisquel, re-bases itself on each new Ubuntu release.

Project goals
=============

-   Obviously, support as much hardware as possible (within the limitations
    imposed by GNU FSDG, and using what coreboot happens to have in its source
    tree - Canoeboot also heavily patches coreboot, sometimes adding new
    mainboards out-of-tree).
-   *Make coreboot easy to use*. Coreboot is notoriously difficult
    to install, due to an overall lack of user-focused documentation
    and support. Most people will simply give up before attempting to
    install coreboot. Canoeboot's automated build system and user-friendly
    installation instructions solves this problem.

Canoeboot attempts to bridge this divide by providing a build system
automating much of the coreboot image creation and customization.
Secondly, the project produces documentation aimed at non-technical users.
Thirdly, the project attempts to provide excellent user support via IRC.

Canoeboot already comes with a payload (GRUB), flashprog and other
needed parts. Everything is fully integrated, in a way where most of
the complicated steps that are otherwise required, are instead done
for the user in advance.

You can download ROM images for your canoeboot system and install
them without having to build anything from source. If, however, you are
interested in building your own image, the build system makes it relatively
easy to do so.

Not a coreboot fork!
--------------------

Canoeboot is not a fork of coreboot. Every so often, the project
re-bases on the latest version of coreboot, by virtue of maintaining sync
with Libreboot releases (minus un-GNU parts), with the number of custom
patches in use minimized. Tested, *stable* (static) releases are then provided
in Canoeboot, based on specific coreboot revisions.

LICENSE FOR THIS README
=======================

It's just a README file. This README file is released under the terms of the
Creative Commons Zero license, version 1.0 of the license, which you can
read here:

<https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt>

The documentation in Canoeboot will use a mix of other licenses, so you should
check that for more information.
