Canoeboot
=========

Find canoeboot documentation at <https://canoeboot.org/>

The `canoeboot` project provides
[libre](https://writefreesoftware.org/) *boot
firmware* that initializes the hardware (e.g. memory controller, CPU,
peripherals) on specific Intel/AMD x86 and ARM targets, which
then starts a bootloader for your operating system. Linux/BSD are
well-supported. It replaces proprietary BIOS/UEFI firmware. Help is available
via [\#canoeboot IRC](https://web.libera.chat/#canoeboot)
on [Libera](https://libera.chat/) IRC.

Why use Canoeboot?
==================

Why should you use *canoeboot*?
----------------------------

Canoeboot gives you freedoms that you otherwise can't get with most other
boot firmware. It's extremely powerful and configurable for many use cases.

You have rights. The right to privacy, freedom of thought, freedom of speech
and the right to read. In this context, Canoeboot gives you these rights.
Your freedom matters.
[Right to repair](https://vid.puffyan.us/watch?v=Npd_xDuNi9k) matters.
Many people use proprietary (non-libre)
boot firmware, even if they use [a libre OS](https://www.openbsd.org/).
Proprietary firmware often contains backdoors (more info on the FAQ), and it
and can be buggy. The canoeboot project was founded in October 2023,
with the express purpose of making coreboot firmware accessible for
non-technical users.

The `canoeboot` project uses [coreboot](https://www.coreboot.org/) for [hardware
initialisation](https://doc.coreboot.org/getting_started/architecture.html).
Coreboot is notoriously difficult to install for most non-technical users; it
handles only basic initialization and jumps to a separate
[payload](https://doc.coreboot.org/payloads.html) program (e.g.
[GRUB](https://www.gnu.org/software/grub/),
[Tianocore](https://www.tianocore.org/)), which must also be configured.
*The canoeboot software solves this problem*; it is a *coreboot distribution* with
an automated build system (named *cbmk*) that builds complete *ROM images*, for
more robust installation. Documentation is provided.

How does Canoeboot differ from coreboot?
========================================

In the same way that *Debian* is a GNU+Linux distribution, `canoeboot` is
a *coreboot distribution*. If you want to build a ROM image from scratch, you
otherwise have to perform expert-level configuration of coreboot, GRUB and
whatever other software you need, to prepare the ROM image. With *canoeboot*,
you can literally download from Git or a source archive, and run `make`, and it
will build entire ROM images. An automated build system, named `cbmk`
(Canoeboot MaKe), builds these ROM images automatically, without any user input
or intervention required. Configuration has already been performed in advance.

If you were to build regular coreboot, without using canoeboot's automated
build system, it would require a lot more intervention and decent technical
knowledge to produce a working configuration.

Regular binary releases of `canoeboot` provide these
ROM images pre-compiled, and you can simply install them, with no special
knowledge or skill except the ability to follow installation instructions
and run commands BSD/Linux.

Canoeboot vs Libreboot
----------------------

Libreboot and Canoeboot are both lead by the same founder, Leah Rowe, kept in
sync whenever feasible; for each Libreboot release, a Canoeboot release follows.

Canoeboot is a fork of [Libreboot](https://libreboot.org/), provided as a proof
of concept demonstrating Libreboot in its current state per release, while
removing any and all parts that do not comply with the GNU Free System
Distribution Guideline (GNU FSDG). GNU FSDG is the policy that Libreboot used to
be based on, but Libreboot adopted a more pragmatic [Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html) in November 2022, resulting
in greater hardware support.

Canoeboot supports fewer mainboards than Libreboot,
because it takes a hardline approach by limiting itself only to those mainboards
which can be booted entirely without binary blob code *in the main boot flash*.
The distinction of *in the main boot flash* is extremely important, because many
examples exist where coreboot interacts with certain proprietary software; for
example, the EC (Embedded Controller) firmware on some ThinkPads that both
Libreboot and Canoeboot support. GNU FSDG is full of contradictions like this;
for example, the HP Elitebooks supported by Libreboot 20231021 use proprietary
EC firmware too, but it's in the main boot flash, whereas on many other machines
it would be on a separate chip, and not the main boot flash.

Coreboot can boot with Free Software exclusively on many boards, but quite a few
more mainboards exist where that is not the case. For example, a given mainboard
may require a blob to configure the memory controller. Libreboot provides as few
binary blobs as possible while allowing any mainboard to be supported, whereas
Canoeboot provides *no* binary blobs and thus can only support a limited subset
of what coreboot supports.

Libreboot started with the no-blob policy, in December 2013. In December 2020,
another project lead by Leah Rowe was created, named *osboot*. The osboot
project had the same policy as current Libreboot, and was later merged into
Libreboot, during November 2022. Thus, Libreboot *became* osboot, and it has
improved substantially since then. This was done, in an effort to bring coreboot
to more people, since Libreboot was much better established at that point. Since
then, the decision has proven to be quite successful, with many more users.

While the no-blob approach may seem noble, it stifles the adopt of Free
Software, in this case coreboot, by alienating those users who like the idea of
free software but do not have such "pure" hardware. The FSF's definition of
purity (in this context) is deeply flawed, as it contains many contradictions.

Binary blobs are *bad*. Free software is *good*, and everyone deserves to have
freedom in their computing. The irony is that there are some who would denounce
the current Libreboot project, under the logic that it is *enabling* proprietary
software, but this is not so; Libreboot is *removing* proprietary software, when
you install it.

Therefore, the nuance in the type of thinking behind *Libreboot* policy is that
while proprietary software *is* bad, and should be avoided, that choice may not
always be available; in such cases then, it can be used but with the stipulation
that it be replaced (with free software) at a future date, and that strong
education is given about it in the meantime. Indeed, many projects out there,
for example many Linux and/or GNU-based systems, will *include* such proprietary
software (such as wifi firmware blobs) and *not tell you about it*, or not
clearly document its existence. Libreboot very clearly documents what and where
these blobs are, in this document:
<https://libreboot.org/freedom-status.html>

The mentality behind Libreboot policy is that *some* software freedom is better
than none. For example, if you're running a ThinkPad X230 or T440p, both of these
can boot entirely without binary blobs in coreboot, but you do still need the
neutered Intel ME image outside of that (in the ME region of the flash, whereas
coreboot goes in the BIOS region). More information about that is available,
on the Freedom Status page of the Libreboot project:
<https://libreboot.org/freedom-status.html>

Libreboot still supports all of the same mainboards that Canoeboot supports, and
provides the exact same configuration as options in each release. In other words,
the no-blob configuration is still possible in Libreboot, and that will always
be true, when possible on a given mainboard. Libreboot's *preference* is
precisely to promote Free Software, hence the Binary Blob Reduction Policy.
Again for posterity, here is a link to the text of that policy:
<https://libreboot.org/news/policy.html>

By contrast, many projects (that handle blobs) do not have such a stipulation.
They either have no policy, a loosely defined policy, or they actively
encourage the use of blobs. Libreboot does not encourage use of binary blobs,
unless absolutely necessary on a given mainboard; whereas, a project like
Debian will provide the `nouveau` driver for nvidia graphics cards, while also
providing the binary drivers from Nvidia (which is proprietary); if Libreboot
were to become a Debian, it would only recommend the `nouveau` driver if that
works well enough. And now an example closer to home: in Libreboot, let's say
you have a board with Intel graphics. Libreboot provides free initialisation
of the framebuffer (using coreboot's `libgfxinit`), which has some limitations
but does work well with Linux/BSD, and you could add Tianocore for an efi
framebuffer if you wanted; an alternative to this is the proprietary VGA Option
ROM from intel, which can provide full VGA mode setting as a BIOS callback,
which would enable more operating systems (e.g. ReactOS, games in FreeDOS, etc).
Libreboot does not provide the Intel VGA ROM, because it is not needed. Linux
and BSD are fine, you you can play DOS games in dosbox if you really want to.
You do not need to boot DOOM on FreeDOS.

To summarise the above point:
if Libreboot had a policy like the Debian one, it would provide that VGA ROM.
Libreboot is not Debian. But it does not follow GNU's hardline approach either.
Both approaches are bad. Libreboot policy is based on FSF ideology, but without
dogma. So, the difference with Canoeboot is that it *also implements the dogma*.
The FSF and GNU dogma is that proprietary software must *always* be avoided,
under all circumstances. Libreboot does not implement such dogma.

The purpose of Canoeboot is therefore to demonstrate what can be done under
such dogma, while providing criticism of it in favour of Libreboot policy.
The Libreboot policy is correct because it provides more software freedom
overall to more people. In any software community, a certain fixed percentage
of people will become programmers, and so bringing coreboot to more people will
inevitably lead to more people becoming coreboot developers; this may then
encourage more people to reverse engineer the blobs to produce free source code.

To put the above point another way:
the GNU FSDG policy, dictated by the FSF, is causing *active harm* to the
adoption of Free Software. For example, if a Windows user is introduced to Linux
for the first time, but they are introduced to an FSDG-compliant distro like
Trisquel, they may find that certain hardware doesn't work. They aren't master
hackers, they are clueless novices, and everything needs to work or they are
going to ditch Linux and keep using Windows. That is just a fact, with most
people, even principled people who believe in the ideals. When someone is just
starting out, they need the experience to be as smooth as possible.

That novice user could one day reverse engineer graphics drivers and bring about
the next revolution in software freedom, but a bad early experience will deter
them early on, and thus stifle future development of free software. This is the
argument put forward by Libreboot, as of November 2022 with the merging of osboot.

tl;dr just imagine a universe where osboot never existed, and know that
Canoeboot is essentially what Libreboot *might* have been at this point in
time. It is a representation of what *was*, and what could have been. Canoeboot
is one possible answer to that *what if*, although the actual universe where
osboot didn't exist may not have produced exactly the same result as Canoeboot
today; it could be slightly different. Libreboot butterflies...

Libreboot today is far superior to Canoeboot, and it's what you should use, but
there are a few who prefer the old Libreboot policy, who absolutely do not want
any proprietary software even to be present, let alone installed and/or
executed. Thus, canoeboot was born.

Canoeboot provides the same level of automation that Libreboot does in the
downloading, patching and building of coreboot ROM images, but it excludes any
binary blobs that may otherwise be present in upstream projects. Canoeboot will
also remove non-blobs that are still proprietary software; there are some cases
where software may be provided by a vendor with source code, but under some
license that puts restrictions on its use or distribution. Canoeboot will only
knowingly provide software that adheres to the GNU Free Software Definition, in
compliance with the GNU Free System Distribution Guidelines.

Project goals
=============

-   *Support as much hardware as possible!* (within the restrictions imposed
    by GNU FSDG policy)
-   *Make coreboot easy to use*. Coreboot is notoriously difficult
    to install, due to an overall lack of user-focused documentation
    and support. Most people will simply give up before attempting to
    install coreboot. Canoeboot's automated build system and user-friendly
    installation instructions solves this problem.

Canoeboot attempts to bridge this divide by providing a build system
automating much of the coreboot image creation and customization.
Secondly, the project produces documentation aimed at non-technical users.
Thirdly, the project attempts to provide excellent user support via IRC.

Canoeboot already comes with a payload (GRUB), flashrom and other
needed parts. Everything is fully integrated, in a way where most of
the complicated steps that are otherwise required, are instead done
for the user in advance.

You can download ROM images for your canoeboot system and install
them without having to build anything from source. If, however, you are
interested in building your own image, the build system makes it relatively
easy to do so.

Not a coreboot fork!
--------------------

Canoeboot is not a fork of coreboot. Every so often, the project
re-bases on the latest version of coreboot, with the number of custom
patches in use minimized. Tested, *stable* (static) releases are then provided
in Canoeboot, based on specific coreboot revisions.

LICENSE FOR THIS README
=======================

It's just a README file. This README file is released under the terms of the
Creative Commons Zero license, version 1.0 of the license, which you can
read here:

<https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt>

The documentation in Canoeboot will use a mix of other licenses, so you should
check that for more information.
