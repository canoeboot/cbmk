From b14bc4a5d8dc31294dde4ee9ce8008d763c695de Mon Sep 17 00:00:00 2001
From: Alper Nebi Yasak <alpernebiyasak@gmail.com>
Date: Fri, 13 Oct 2023 14:06:26 +0300
Subject: [PATCH] HACK: rockchip: Remove binman image descriptions

For Rockchip boards binman tries to build SPI and MMC images that
require an externally built BL31 file to be provided, and the build
fails otherwise. This is not really as configurable as it should be.

Some downstreams only care about build outputs for U-Boot proper. As a
hack to make sure they can do so without building BL31, remove the
binman image descriptions from the device-tree sources. However,
BINMAN_FDT expects these to be present and has to be disabled as well.

Signed-off-by: Alper Nebi Yasak <alpernebiyasak@gmail.com>
---
 arch/arm/dts/rk3288-u-boot.dtsi     |  24 ----
 arch/arm/dts/rk3399-gru-u-boot.dtsi |   6 -
 arch/arm/dts/rk3399-u-boot.dtsi     |  25 ----
 arch/arm/dts/rockchip-u-boot.dtsi   | 186 ----------------------------
 4 files changed, 241 deletions(-)

diff --git a/arch/arm/dts/rk3288-u-boot.dtsi b/arch/arm/dts/rk3288-u-boot.dtsi
index c4c5a2d225c4..432ab92d97c9 100644
--- a/arch/arm/dts/rk3288-u-boot.dtsi
+++ b/arch/arm/dts/rk3288-u-boot.dtsi
@@ -55,30 +55,6 @@ noc: syscon@ffac0000 {
 	};
 };
 
-#if defined(CONFIG_ROCKCHIP_SPI_IMAGE) && defined(CONFIG_HAS_ROM)
-&binman {
-	rom {
-		filename = "u-boot.rom";
-		size = <0x400000>;
-		pad-byte = <0xff>;
-
-		mkimage {
-			args = "-n rk3288 -T rkspi";
-			u-boot-spl {
-			};
-		};
-		u-boot-img {
-			offset = <0x20000>;
-		};
-		u-boot {
-			offset = <0x300000>;
-		};
-		fdtmap {
-		};
-	};
-};
-#endif
-
 &bus_intmem {
 	ddr_sram: ddr-sram@1000 {
 		compatible = "rockchip,rk3288-ddr-sram";
diff --git a/arch/arm/dts/rk3399-gru-u-boot.dtsi b/arch/arm/dts/rk3399-gru-u-boot.dtsi
index b1604a6872c0..54296b4d7a1c 100644
--- a/arch/arm/dts/rk3399-gru-u-boot.dtsi
+++ b/arch/arm/dts/rk3399-gru-u-boot.dtsi
@@ -15,12 +15,6 @@ config {
 	};
 };
 
-&binman {
-	rom {
-		size = <0x800000>;
-	};
-};
-
 &cros_ec {
 	ec-interrupt = <&gpio0 RK_PA1 GPIO_ACTIVE_LOW>;
 };
diff --git a/arch/arm/dts/rk3399-u-boot.dtsi b/arch/arm/dts/rk3399-u-boot.dtsi
index 3423b882c437..0bf4c481b39a 100644
--- a/arch/arm/dts/rk3399-u-boot.dtsi
+++ b/arch/arm/dts/rk3399-u-boot.dtsi
@@ -60,31 +60,6 @@ pmusgrf: syscon@ff330000 {
 
 };
 
-#if defined(CONFIG_ROCKCHIP_SPI_IMAGE) && defined(CONFIG_HAS_ROM)
-&binman {
-	multiple-images;
-	rom {
-		filename = "u-boot.rom";
-		size = <0x400000>;
-		pad-byte = <0xff>;
-
-		mkimage {
-			args = "-n rk3399 -T rkspi";
-			u-boot-spl {
-			};
-		};
-		u-boot-img {
-			offset = <0x40000>;
-		};
-		u-boot {
-			offset = <0x300000>;
-		};
-		fdtmap {
-		};
-	};
-};
-#endif /* CONFIG_ROCKCHIP_SPI_IMAGE && CONFIG_HAS_ROM */
-
 &cru {
 	bootph-all;
 };
diff --git a/arch/arm/dts/rockchip-u-boot.dtsi b/arch/arm/dts/rockchip-u-boot.dtsi
index be2658e8ef18..3d55553e4401 100644
--- a/arch/arm/dts/rockchip-u-boot.dtsi
+++ b/arch/arm/dts/rockchip-u-boot.dtsi
@@ -10,189 +10,3 @@ binman: binman {
 		multiple-images;
 	};
 };
-
-#ifdef CONFIG_SPL
-&binman {
-	simple-bin {
-		filename = "u-boot-rockchip.bin";
-		pad-byte = <0xff>;
-
-		mkimage {
-			filename = "idbloader.img";
-			args = "-n", CONFIG_SYS_SOC, "-T", "rksd";
-			multiple-data-files;
-
-#ifdef CONFIG_ROCKCHIP_EXTERNAL_TPL
-			rockchip-tpl {
-			};
-#elif defined(CONFIG_TPL)
-			u-boot-tpl {
-			};
-#endif
-			u-boot-spl {
-			};
-		};
-
-#if defined(CONFIG_SPL_FIT) && (defined(CONFIG_ARM64) || defined(CONFIG_SPL_OPTEE_IMAGE))
-		fit: fit {
-#ifdef CONFIG_ARM64
-			description = "FIT image for U-Boot with bl31 (TF-A)";
-#else
-			description = "FIT image with OP-TEE";
-#endif
-			#address-cells = <1>;
-			fit,fdt-list = "of-list";
-			filename = "u-boot.itb";
-			fit,external-offset = <CONFIG_FIT_EXTERNAL_OFFSET>;
-			fit,align = <512>;
-			offset = <CONFIG_SPL_PAD_TO>;
-			images {
-				u-boot {
-					description = "U-Boot";
-					type = "standalone";
-					os = "U-Boot";
-#ifdef CONFIG_ARM64
-					arch = "arm64";
-#else
-					arch = "arm";
-#endif
-					compression = "none";
-					load = <CONFIG_TEXT_BASE>;
-					entry = <CONFIG_TEXT_BASE>;
-					u-boot-nodtb {
-					};
-#ifdef CONFIG_SPL_FIT_SIGNATURE
-					hash {
-						algo = "sha256";
-					};
-#endif
-				};
-
-#ifdef CONFIG_ARM64
-				@atf-SEQ {
-					fit,operation = "split-elf";
-					description = "ARM Trusted Firmware";
-					type = "firmware";
-					arch = "arm64";
-					os = "arm-trusted-firmware";
-					compression = "none";
-					fit,load;
-					fit,entry;
-					fit,data;
-
-					atf-bl31 {
-					};
-#ifdef CONFIG_SPL_FIT_SIGNATURE
-					hash {
-						algo = "sha256";
-					};
-#endif
-				};
-				@tee-SEQ {
-					fit,operation = "split-elf";
-					description = "TEE";
-					type = "tee";
-					arch = "arm64";
-					os = "tee";
-					compression = "none";
-					fit,load;
-					fit,entry;
-					fit,data;
-
-					tee-os {
-						optional;
-					};
-#ifdef CONFIG_SPL_FIT_SIGNATURE
-					hash {
-						algo = "sha256";
-					};
-#endif
-				};
-#else
-				op-tee {
-					description = "OP-TEE";
-					type = "tee";
-					arch = "arm";
-					os = "tee";
-					compression = "none";
-					load = <(CFG_SYS_SDRAM_BASE + 0x8400000)>;
-					entry = <(CFG_SYS_SDRAM_BASE + 0x8400000)>;
-
-					tee-os {
-					};
-#ifdef CONFIG_SPL_FIT_SIGNATURE
-					hash {
-						algo = "sha256";
-					};
-#endif
-				};
-#endif
-
-				@fdt-SEQ {
-					description = "fdt-NAME";
-					compression = "none";
-					type = "flat_dt";
-#ifdef CONFIG_SPL_FIT_SIGNATURE
-					hash {
-						algo = "sha256";
-					};
-#endif
-				};
-			};
-
-			configurations {
-				default = "@config-DEFAULT-SEQ";
-				@config-SEQ {
-					description = "NAME.dtb";
-					fdt = "fdt-SEQ";
-#ifdef CONFIG_ARM64
-					fit,firmware = "atf-1", "u-boot";
-#else
-					fit,firmware = "op-tee", "u-boot";
-#endif
-					fit,loadables;
-				};
-			};
-		};
-#else
-		u-boot-img {
-			offset = <CONFIG_SPL_PAD_TO>;
-		};
-#endif
-	};
-
-#ifdef CONFIG_ROCKCHIP_SPI_IMAGE
-	simple-bin-spi {
-		filename = "u-boot-rockchip-spi.bin";
-		pad-byte = <0xff>;
-
-		mkimage {
-			filename = "idbloader-spi.img";
-			args = "-n", CONFIG_SYS_SOC, "-T", "rkspi";
-			multiple-data-files;
-
-#ifdef CONFIG_ROCKCHIP_EXTERNAL_TPL
-			rockchip-tpl {
-			};
-#elif defined(CONFIG_TPL)
-			u-boot-tpl {
-			};
-#endif
-			u-boot-spl {
-			};
-		};
-
-#if defined(CONFIG_ARM64) || defined(CONFIG_SPL_OPTEE_IMAGE)
-		fit {
-			type = "blob";
-			filename = "u-boot.itb";
-#else
-		u-boot-img {
-#endif
-			/* Sync with u-boot,spl-payload-offset if present */
-			offset = <CONFIG_SYS_SPI_U_BOOT_OFFS>;
-		};
-	};
-#endif /* CONFIG_ROCKCHIP_SPI_IMAGE */
-};
-#endif /* CONFIG_SPL */
-- 
2.42.0

